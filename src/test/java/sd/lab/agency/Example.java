package sd.lab.agency;

import org.junit.Test;
import sd.lab.agency.impl.BaseAgent;
import sd.lab.agency.impl.LocalEnvironment;

import java.time.Duration;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeoutException;

public class Example {
    private static final Duration MAX_WAIT = Duration.ofSeconds(2);
    @Test
    public void multipleAgentsDoingBlockingOperationsOnSingleThread() throws InterruptedException, ExecutionException, TimeoutException {
        final Environment env = new LocalEnvironment(Executors.newSingleThreadExecutor());

        env.createAgent(Receiver.class, "Bob").start();
        env.createAgent(Receiver.class, "Carl").start();
        env.createAgent(Sender.class, "Alice").start();

        env.awaitAllAgentsStop(MAX_WAIT)
                .shutdown()
                .awaitShutdown(MAX_WAIT);
    }

    static class Receiver extends BaseAgent {

        public Receiver(String name) {
            super(name);
        }

        @Override
        public void onRun() throws Exception {
            getEnvironment().getTextualSpace("someTS")
                    .rd("message\\{.*?\\}")
                    .thenApplyAsync(t -> stop(), getEngine());
            pause();
        }
    }

    static class Sender extends BaseAgent {

        public Sender(String name) {
            super(name);
        }

        @Override
        public void onRun() throws Exception {
            getEnvironment().getTextualSpace("someTS")
                    .out("message{hello}")
                    .thenApplyAsync(t -> stop(), getEngine());
            pause();
        }
    }
}
