package sd.lab.agency.behavioural;

import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import sd.lab.agency.Agent;
import sd.lab.agency.Environment;
import sd.lab.agency.behavioural.impl.BaseBehaviouralAgent;
import sd.lab.agency.behavioural.messages.Message;
import sd.lab.agency.impl.LocalEnvironment;
import sd.lab.linda.textual.StringTuple;
import sd.lab.test.ConcurrentTestHelper;

import java.time.Duration;
import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@RunWith(Parameterized.class)
public class TestBehaviouralAgent {

    private static final Duration MAX_WAIT = Duration.ofSeconds(3);
    private final int testIndex;
    protected ConcurrentTestHelper test;
    protected Random rand;
    protected Environment mas;


    public TestBehaviouralAgent(Integer i) {
        testIndex = i;
    }

    @Parameterized.Parameters
    public static Iterable<Integer> data() {
        return IntStream.range(0, 5).boxed().collect(Collectors.toList());
    }

    @Before
    public void setUp() {
        test = new ConcurrentTestHelper();
        rand = new Random();
        mas = new LocalEnvironment(Executors.newSingleThreadExecutor(), getClass().getSimpleName() + "-Environment-" + testIndex);
    }

    @After
    public void tearDown() throws InterruptedException {
        mas.shutdown().awaitShutdown(MAX_WAIT);
    }

    @Test
    public void testOneShot() throws Exception {
        final List<Integer> xs = new LinkedList<>();

        mas.registerAgent(new BaseBehaviouralAgent("testOneShot-" + testIndex) {
            @Override
            public void setup() {
                Behaviour.of(() -> xs.add(1))
                        .andThen(Agent::stop)
                        .addTo(this);
            }
        }).start();

        mas.awaitAllAgentsStop(MAX_WAIT);

        Assert.assertEquals(xs, Collections.singletonList(1));
    }

    @Test
    public void testSequence1() throws Exception {
        final List<Integer> xs = new LinkedList<>();

        mas.registerAgent(new BaseBehaviouralAgent("testSequence1-" + testIndex) {
            @Override
            public void setup() {
                Behaviour.sequence(
                        Behaviour.of(() -> xs.add(1)),
                        Behaviour.of(() -> xs.add(2)),
                        Behaviour.of(() -> xs.add(3))
                ).andThen(Agent::stop)
                .addTo(this);
            }
        }).start();

        mas.awaitAllAgentsStop(MAX_WAIT);

        Assert.assertEquals(Arrays.asList(1, 2, 3), xs);
    }

    @Test
    public void testSequence2() throws Exception {
        final List<Integer> xs = new LinkedList<>();

        mas.registerAgent(new BaseBehaviouralAgent("testSequence2-" + testIndex) {
            @Override
            public void setup() {
                Behaviour.of(() -> xs.add(1))
                        .andThen(() -> xs.add(2))
                        .andThen(() -> xs.add(3))
                        .andThen(Agent::stop)
                        .addTo(this);
            }
        }).start();

        mas.awaitAllAgentsStop(MAX_WAIT);

        Assert.assertEquals(Arrays.asList(1, 2, 3), xs);
    }

    @Test
    public void testJoin() throws Exception {
        final List<Object> xs = new LinkedList<>();

        mas.registerAgent(new BaseBehaviouralAgent("testJoin-" + testIndex) {
            @Override
            public void setup() {
                Behaviour.allOf(
                        Behaviour.of(() -> xs.add(1))
                                .andThen(() -> xs.add(2))
                                .andThen(() -> xs.add(3)),

                        Behaviour.of(() -> xs.add("a"))
                                .andThen(() -> xs.add("b"))
                                .andThen(() -> xs.add("c"))
                                .andThen(() -> xs.add("d"))
                ).andThen(Agent::stop)
                .addTo(this);
            }
        }).start();

        mas.awaitAllAgentsStop(MAX_WAIT);

        Assert.assertEquals(Arrays.asList(1, "a", 2, "b", 3, "c", "d"), xs);
    }

    @Test
    public void testParallel() throws Exception {
        final List<Object> xs = new LinkedList<>();

        mas.registerAgent(new BaseBehaviouralAgent("testParallel-" + testIndex) {
            @Override
            public void setup() {
                Behaviour.anyOf(
                        Behaviour.of(() -> xs.add(1)).
                                andThen(() -> xs.add(2)),

                        Behaviour.of(() -> xs.add("a"))
                                .andThen(() -> xs.add("b"))
                                .andThen(() -> xs.add("c"))
                                .andThen(() -> xs.add("d"))
                ).andThen(Agent::stop)
                .addTo(this);
            }
        }).start();

        mas.awaitAllAgentsStop(MAX_WAIT);

        Assert.assertEquals(Arrays.asList(1, "a", 2), xs);
    }

    @Test
    public void testDoWhile() throws Exception {
        final List<Object> xs = new LinkedList<>();

        mas.registerAgent(new BaseBehaviouralAgent("testDoWhile-" + testIndex) {
            @Override
            public void setup() {
                Behaviour.of(() -> xs.add(1))
                        .andThen(() -> xs.add(2))
                        .andThen(() -> xs.add(3))
                        .repeatWhile(() -> xs.size() < 7)
                        .andThen(Agent::stop)
                        .addTo(this);
            }
        }).start();

        mas.awaitAllAgentsStop(Duration.ofMillis(Long.MAX_VALUE));

        Assert.assertEquals(Arrays.asList(1, 2, 3, 1, 2, 3, 1, 2, 3), xs);
    }

    @Test
    public void testWait() throws Exception {
        final Duration toWait = Duration.ofSeconds(1);

        final OffsetDateTime start = OffsetDateTime.now();

        mas.registerAgent(new BaseBehaviouralAgent("testWait-" + testIndex) {

            @Override
            public void setup() {
                Behaviour.wait(toWait)
                        .andThen(Agent::stop)
                        .addTo(this);
            }
        }).start();

        mas.awaitAllAgentsStop(Duration.ofMillis(Long.MAX_VALUE));

        Assert.assertTrue(ChronoUnit.MILLIS.between(start, OffsetDateTime.now()) >= toWait.toMillis());
    }

    @Test
    public void testDoForAWhile() throws Exception {
        final Duration toWait = Duration.ofSeconds(1);
        final List<Integer> xs = new LinkedList<>();
        final OffsetDateTime start = OffsetDateTime.now();
        final AtomicInteger i = new AtomicInteger(0);

        mas.registerAgent(new BaseBehaviouralAgent("testDoForAWhile-" + testIndex) {

            @Override
            public void setup() {
                Behaviour.anyOf(
                        Behaviour.wait(toWait),
                        Behaviour.of(() -> xs.add(i.getAndIncrement())).repeatForEver()
                ).andThen(Agent::stop)
                .addTo(this);
            }
        }).start();

        mas.awaitAllAgentsStop(Duration.ofMillis(Long.MAX_VALUE));

        Assert.assertTrue(ChronoUnit.MILLIS.between(start, OffsetDateTime.now()) >= toWait.toMillis());
        Assert.assertTrue(i.get() > 0);
        Assert.assertEquals(IntStream.range(0, i.get()).boxed().collect(Collectors.toList()), xs);
    }

    @Test
    public void testLinda() throws Exception {
        final List<Object> xs = new LinkedList<>();

        mas.registerAgent(new BaseBehaviouralAgent("testLinda-Alice-" + testIndex) {
            @Override
            public void setup() {
                Behaviour.lindaOperation("testLinda-" + testIndex,
                        tupleSpace -> tupleSpace.out("msg{payload}"),
                        writtenTuple -> xs.add(writtenTuple)
                ).andThen(Agent::stop)
                .addTo(this);
            }
        }).start();

        mas.registerAgent(new BaseBehaviouralAgent("testLinda-Bob-" + testIndex) {
            @Override
            public void setup() {
                Behaviour.lindaOperation("testLinda-" + testIndex,
                        tupleSpace -> tupleSpace.in("msg\\{.*?}"),
                        takenTuple -> xs.add(takenTuple)
                ).andThen(Agent::stop)
                .addTo(this);
            }
        }).start();

        mas.awaitAllAgentsStop(Duration.ofMillis(Long.MAX_VALUE));

        Assert.assertEquals(
                Arrays.asList(StringTuple.of("msg{payload}"), StringTuple.of("msg{payload}")),
                xs
        );
    }

    @Test
    public void testSendReceive() throws Exception {
        final List<Message> xs = new LinkedList<>();

        mas.registerAgent(new BaseBehaviouralAgent("testSendReceive-Alice-" + testIndex) {
            @Override
            public void setup() {
                Behaviour.send("testSendReceive-Bob-" + testIndex, "hello")
                        .andThen(Agent::stop)
                        .addTo(this);
            }
        }).start();

        mas.registerAgent(new BaseBehaviouralAgent("testSendReceive-Bob-" + testIndex) {
            @Override
            public void setup() {
                Behaviour.receive(msg -> xs.add(msg))
                        .andThen(Agent::stop)
                        .addTo(this);
            }
        }).start();

        mas.awaitAllAgentsStop(Duration.ofMillis(Long.MAX_VALUE));

        Assert.assertEquals(
                Collections.singletonList(
                        Message.of(
                                "testSendReceive-Alice-" + testIndex + "@" + mas.getName(),
                                "testSendReceive-Bob-" + testIndex + "@" + mas.getName(),
                                "hello"
                        )
                ),
                xs
        );
    }

    @Test
    public void testPingPong1() throws Exception {
        final List<Message> xs = new LinkedList<>();

        mas.registerAgent(new BaseBehaviouralAgent("testPingPong1-Ping-" + testIndex) {
            @Override
            public void setup() {
                Behaviour.send("testPingPong1-Pong-" + testIndex, "ping")
                        .andThen(Behaviour.receive(msg -> {
                            if (!msg.getContent().equals("pong")) {
                                throw new IllegalStateException();
                            }
                            xs.add(msg);
                        }))
                        .andThen(Agent::stop)
                        .addTo(this);
            }
        }).start();

        mas.registerAgent(new BaseBehaviouralAgent("testPingPong1-Pong-" + testIndex) {
            @Override
            public void setup() {
                Behaviour.receive(msg -> {
                    if (!msg.getContent().equals("ping")) {
                        throw new IllegalStateException();
                    }
                    xs.add(msg);
                }).andThen(Behaviour.send("testPingPong1-Ping-" + testIndex, "pong"))
                .andThen(Agent::stop)
                .addTo(this);
            }
        }).start();

        mas.awaitAllAgentsStop(Duration.ofMillis(Long.MAX_VALUE));

        Assert.assertEquals(2, xs.size());

        Assert.assertEquals(
                Arrays.asList("ping", "pong"),
                xs.stream().map(Message::getContent).collect(Collectors.toList())
        );
    }

    @Test
    public void testPingPongN() throws Exception {
        final int n = 5;
        final List<Message> xs = new LinkedList<>();

        mas.registerAgent(new BaseBehaviouralAgent("testPingPongN-Ping-" + testIndex) {
            int i = 0;

            @Override
            public void setup() {
                Behaviour.send("testPingPongN-Pong-" + testIndex, "ping")
                        .andThen(Behaviour.receive(msg -> {
                            if (!msg.getContent().equals("pong")) {
                                throw new IllegalStateException();
                            }
                            xs.add(msg);
                        }))
                        .repeatWhile(() -> ++i < n)
                        .andThen(Agent::stop)
                        .addTo(this);
            }
        }).start();

        mas.registerAgent(new BaseBehaviouralAgent("testPingPongN-Pong-" + testIndex) {
            int j = 0;

            @Override
            public void setup() {
                Behaviour.receive(msg -> {
                    if (!msg.getContent().equals("ping")) {
                        throw new IllegalStateException();
                    }
                    xs.add(msg);
                }).andThen(Behaviour.send("testPingPongN-Ping-" + testIndex, "pong"))
                .repeatWhile(() -> ++j < n)
                .andThen(Agent::stop)
                .addTo(this);
            }
        }).start();

        mas.awaitAllAgentsStop(Duration.ofMillis(Long.MAX_VALUE));

        Assert.assertEquals(10, xs.size());

        Assert.assertEquals(
                IntStream.range(0, 5).boxed().flatMap(i -> Stream.of("ping", "pong")).collect(Collectors.toList()),
                xs.stream().map(Message::getContent).collect(Collectors.toList())
        );
    }


    @Test
    public void testAgentDoesNothingByDefault() throws Exception {
        final Agent lazy = mas.registerAgent(new BaseBehaviouralAgent("testAgentDoesNothingByDefault" + testIndex) {
            @Override
            public void setup() { }
        }).start();

        try {
            lazy.await(MAX_WAIT);
            Assert.fail();

        } catch (InterruptedException | TimeoutException e) {
            Assert.assertTrue(true);
        }
    }

    @Test
    public void testAgentCreationRequiresAConstructorAcceptingAnAgentId() {
        try {
            Assert.assertEquals(
                    "testAgentCreationRequiresAConstructorAcceptingAnAgentId" + testIndex + "@" + mas.getName(),
                    mas.createAgent(MyAgent1.class, "testAgentCreationRequiresAConstructorAcceptingAnAgentId" + testIndex).getFullName()
            );

            mas.createAgent(MyAgent2.class, "testAgentCreationRequiresAConstructorAcceptingAnAgentId" + testIndex);
            Assert.fail();
        } catch (IllegalArgumentException e) {
            Assert.assertTrue(true);
        }
    }

    static class MyAgent1 extends BaseBehaviouralAgent {

        public MyAgent1(String id) {
            super(id);

            Behaviour.of(this::stop).addTo(this);
        }
    }

    static class MyAgent2 extends BaseBehaviouralAgent {

        public MyAgent2() {
            super("my-name");

            Behaviour.of(this::stop).addTo(this);
        }
    }

}

