package sd.lab.agency.exercises;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import sd.lab.agency.Environment;
import sd.lab.agency.impl.LocalEnvironment;
import sd.lab.exercises.PingAgent;
import sd.lab.exercises.PingAgent2;
import sd.lab.exercises.PongAgent;
import sd.lab.exercises.PongAgent2;
import sd.lab.test.ConcurrentTestHelper;

import java.time.Duration;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@RunWith(Parameterized.class)
public class TestPingPong2 {

    private static final Duration MAX_WAIT = Duration.ofSeconds(2);

    protected ConcurrentTestHelper test;
    protected Environment mas;

    private final int testIndex;

    @Parameterized.Parameters
    public static Iterable<Integer> data() {
        return IntStream.range(0, 5).boxed().collect(Collectors.toList());
    }

    public TestPingPong2(Integer i) {
        testIndex = i;
    }

    @Before
    public void setUp() throws Exception {
        test = new ConcurrentTestHelper();
        // TODO notice that all agents are executed by a single thread in this test suite!
        mas = new LocalEnvironment(Executors.newSingleThreadExecutor(), getClass().getSimpleName() + "-Environment-" + testIndex);
    }

    @After
    public void tearDown() throws InterruptedException, ExecutionException, TimeoutException {
        mas.shutdown().awaitShutdown(MAX_WAIT);
    }

    // TODO readme
    @Test
    public void testPingPongExercise() throws Exception {

        final String pingName = UUID.randomUUID().toString() + "-ping";
        final String pongName = UUID.randomUUID().toString() + "-pong";

        final PingAgent2 ping = (PingAgent2) mas.createAgent(PingAgent2.class, pingName, "testPingPongExercise-" + testIndex).start();

        mas.createAgent(PongAgent2.class, pongName, "testPingPongExercise-" + testIndex).start();

        mas.awaitAllAgentsStop(MAX_WAIT);

        Assert.assertEquals(10, ping.getPongCount());
        Assert.assertTrue(ping.isPingTurn());
    }

}
