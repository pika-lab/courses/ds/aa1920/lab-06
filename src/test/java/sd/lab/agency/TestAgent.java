package sd.lab.agency;

import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import sd.lab.agency.impl.BaseAgent;
import sd.lab.agency.impl.LocalEnvironment;
import sd.lab.test.ConcurrentTestHelper;

import java.time.Duration;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@RunWith(Parameterized.class)
public class TestAgent {

    private static final Duration MAX_WAIT = Duration.ofSeconds(2);

    protected ConcurrentTestHelper test;
    protected Random rand;
    protected Environment mas;

    private final int testIndex;


    @Parameterized.Parameters
    public static Iterable<Integer> data() {
        return IntStream.range(0, 5).boxed().collect(Collectors.toList());
    }

    public TestAgent(Integer i) {
        testIndex = i;
    }

    @Before
    public void setUp() throws Exception {
        test = new ConcurrentTestHelper();
        rand = new Random();
        // TODO notice that all agents are executed by a single thread in this test suite!
        mas = new LocalEnvironment(Executors.newSingleThreadExecutor(), getClass().getSimpleName() + "-Environment-" + testIndex);
    }

    @After
    public void tearDown() throws InterruptedException, ExecutionException, TimeoutException {
        mas.shutdown().awaitShutdown(MAX_WAIT);
    }

    // TODO readme
    @Test
    public void testAgentsFlow() throws Exception {
        final List<Integer> xs = new LinkedList<>();

        mas.registerAgent(new BaseAgent("Alice") {
            int x = 0;

            @Override
            public void onBegin() throws Exception {
                xs.add(-1);
                throw new RuntimeException("Ignore me");
            }

            @Override
            public void onRun() throws Exception {
                if (x < 10) {
                    xs.add(x++);
                } else {
                    throw new Exception("Stop the Agent now!");
                }
            }

            @Override
            public AndThen onUncaughtError(Exception e) {
                if (e instanceof RuntimeException) {
                    xs.add(-1);
                    return AndThen.CONTINUE;
                } else {
                    xs.add(x++);
                    return AndThen.STOP;
                }
            }

            @Override
            public void onEnd() throws Exception {
                xs.add(x);
            }

        }).start();

        mas.awaitAllAgentsStop(MAX_WAIT);

        Assert.assertEquals(
                Arrays.asList(-1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11),
                xs
        );
    }

    // TODO readme
    @Test
    public void testAgentsRestart() throws Exception {
        final List<Integer> xs = new LinkedList<>();

        mas.registerAgent(new BaseAgent("Alice") {
            int x = -1;

            @Override
            public void onBegin() throws Exception {
                xs.add(x);
                x += 2;
                if (x == 1) {
                    throw new RuntimeException("Restart the agent!");
                }
            }

            @Override
            public void onRun() throws Exception {
                if (x == 10) {
                    xs.add(x++);
                    throw new RuntimeException("Restart the agent!");
                } else if (x == 15) {
                    xs.add(x++);
                    throw new Exception("Stop the agent!");
                } else {
                    xs.add(x++);
                }
            }

            @Override
            public AndThen onUncaughtError(Exception e) {
                if (e instanceof RuntimeException) {
                    xs.add(-1);
                    return AndThen.RESTART;
                } else {
                    xs.add(x++);
                    return AndThen.STOP;
                }
            }

            @Override
            public void onEnd() throws Exception {
                xs.add(x);
            }

        }).start();


        mas.awaitAllAgentsStop(MAX_WAIT);

        Assert.assertEquals(
                Arrays.asList(-1, -1, 1, 3, 4, 5, 6, 7, 8, 9, 10, -1, 11, 13, 14, 15, 16, 17),
                xs
        );
    }

    // TODO readme
    @Test
    public void testAgentsRunOnTheSameExecutor() throws Exception {
        final List<String> xs = new LinkedList<>();

        mas.registerAgent(new BaseAgent("Bob") {

            @Override
            public void onRun() throws Exception {
                xs.add("b1");
                getEnvironment().getTextualSpace("testAgentsRunOnTheSameExecutor-" + testIndex)
                        .in("signal\\(.*?\\)")
                        .get();
                xs.add("b2");
                stop();
            }
        }).start();

        mas.registerAgent(new BaseAgent("Alice") {
            @Override
            public void onRun() throws Exception {
                xs.add("a1");
                getEnvironment().getTextualSpace("testAgentsRunOnTheSameExecutor-" + testIndex)
                        .out("signal(now)")
                        .get();
                xs.add("a2");
                stop();
            }
        }).start();

        try {
            mas.awaitAllAgentsStop(MAX_WAIT);
            Assert.fail();
        } catch (TimeoutException e) {
            Assert.assertEquals(
                    Collections.singletonList("b1"),
                    xs
            );
        }
    }

    // TODO readme
    @Test
    public void testAgentsPause() throws Exception {
        final List<String> xs = new LinkedList<>();

        mas.registerAgent(new BaseAgent("Bob") {

            boolean signalReceived = false;

            @Override
            public void onRun() throws Exception {
                if (!signalReceived) {
                    xs.add("b1");
                    getEnvironment().getTextualSpace("testAgentsRunOnTheSameExecutor-" + testIndex)
                            .in("signal\\(.*?\\)")
                            .thenRunAsync(() -> {
                                xs.add("b4");
                                signalReceived = true;
                                resume();
                            }, getEngine());
                    xs.add("b2");
                    pause();
                    xs.add("b3");
                } else {
                    xs.add("b5");
                    stop();
                }
            }
        }).start();

        mas.registerAgent(new BaseAgent("Alice") {
            @Override
            public void onRun() throws Exception {
                xs.add("a1");
                getEnvironment().getTextualSpace("testAgentsRunOnTheSameExecutor-" + testIndex)
                        .out("signal(now)");
                xs.add("a2");
                stop();
            }
        }).start();

        mas.awaitAllAgentsStop(MAX_WAIT);

        Assert.assertEquals(
                Arrays.asList("b1", "b2", "b3", "a1", "a2", "b4", "b5"),
                xs
        );
    }
}
