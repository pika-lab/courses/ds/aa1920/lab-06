package sd.lab.exercises;

import sd.lab.agency.impl.BaseAgent;
import sd.lab.linda.textual.RegexTemplate;
import sd.lab.linda.textual.StringTuple;
import sd.lab.linda.textual.TextualSpace;

import java.util.Objects;
import java.util.regex.Matcher;

public class PongAgent extends BaseAgent {

    private final RegexTemplate msgTemplate;

    private boolean pongTurn = false;
    private final String channelName;
    private TextualSpace channel;

    public PongAgent(String name, String channelName) {
        super(name);
        this.channelName = Objects.requireNonNull(channelName);
        this.msgTemplate = RegexTemplate.of(
                "^\\s*msg\\s*\\{\\s*to:\\s*" + name + "\\s*,\\s*content:\\s*(.*?)\\s*\\}\\s*$"
        );
    }

    @Override
    public void onBegin() throws Exception {
        channel = getEnvironment().getTextualSpace(channelName);
    }

    @Override
    public void onRun() throws Exception {
        if (pongTurn) {
            channel.out("msg{to: PingAgent, content: pong}").thenAcceptAsync(this::onPongWritten, getEngine());
            pause();
        } else {
            channel.in(msgTemplate).thenAcceptAsync(this::onMessageReceived, getEngine());
            pause();
        }
    }

    private void onMessageReceived(StringTuple tuple) {
        log("Received message %s", tuple);
        final Matcher m = msgTemplate.getRegex().matcher(tuple.getValue());
        if (m.matches()) {
            final String content = m.group(1);
            if (content.equalsIgnoreCase("ping")) {
                log("Receive PING");
                pongTurn = true;
                resume();
            } else if (content.equalsIgnoreCase("stop")) {
                log("Invalid message. I'm out");
                stop();
            }
        } else {
            log("THIS SHOULD NEVER HAPPEN");
            stop();
        }
    }

    private void onMessageReSent(StringTuple stringTuple) {
        log("Re-sent message %s: it was not directed to me", stringTuple);
        resume();
    }

    private void onPongWritten(StringTuple tuple) {
        log("Sent PONG");
        pongTurn = false;
        resume();
    }

}
