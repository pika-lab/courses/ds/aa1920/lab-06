package sd.lab.exercises;

import sd.lab.agency.impl.BaseAgent;
import sd.lab.linda.textual.RegexTemplate;
import sd.lab.linda.textual.StringTuple;
import sd.lab.linda.textual.TextualSpace;

import java.util.Objects;
import java.util.regex.Matcher;

// TODO notice me!
public class PongAgent2 extends BaseAgent {

    // WTF?! https://regex101.com/r/dGHAjw/1/
    private final RegexTemplate msgTemplate, onlineTemplate;

    private int pingCount = 0;
    private boolean discovered = false;
    private boolean pongTurn = false;
    private final String channelName;
    private TextualSpace channel;
    private String pingName;

    public PongAgent2(String name, String channelName) {
        super(name);
        this.channelName = Objects.requireNonNull(channelName);
        // WTF?! https://regex101.com/r/dGHAjw/2
        this.msgTemplate = RegexTemplate.of(
                "^\\s*msg\\s*\\{\\s*to:\\s*" + name + "\\s*,\\s*content:\\s*(.*?)\\s*\\}\\s*$"
        );
        this.onlineTemplate = RegexTemplate.of(
                "^\\s*online\\s*\\{\\s*name:\\s*(?!" + name + ")(.*?)\\s*}\\s*$"
        );
    }

    @Override
    public void onBegin() throws Exception {
        channel = getEnvironment().getTextualSpace(channelName);
    }

    @Override
    public void onRun() throws Exception {
        if (!discovered) {
            channel.out("online{name:" + getLocalName() + "}");
            channel.in(onlineTemplate).thenAcceptAsync(this::onPeerNameFound, getEngine());
            pause();
        } else if (pongTurn) {
            channel.out("msg{to: " + pingName + ", content: pong}").thenAcceptAsync(this::onPongWritten, getEngine());
            pause();
        } else {
            channel.in(msgTemplate).thenAcceptAsync(this::onMessageReceived, getEngine());
            pause();
        }
    }

    private void onPeerNameFound(StringTuple tuple) {
        log("Fount online peer: %s", tuple);
        final Matcher m = onlineTemplate.getRegex().matcher(tuple.getValue());
        if (m.matches()) {
            final String content = m.group(1);
            log("Discovered ping name: %s", content);
            pingName = content;
            discovered = true;
            resume();
        } else {
            log("THIS SHOULD NEVER HAPPEN");
            stop();
        }
    }

    private void onPongWritten(StringTuple tuple) {
        log("Sent PONG");
        pongTurn = false;
        resume();
    }

    private void onMessageReceived(StringTuple tuple) {
        log("Received message %s", tuple);
        final Matcher m = msgTemplate.getRegex().matcher(tuple.getValue());
        if (m.matches()) {
            final String content = m.group(1);
            if (content.equalsIgnoreCase("ping")) {
                log("Receive PING");
                pongTurn = true;
                resume();
            } else if (content.equalsIgnoreCase("stop")) {
                log("Invalid message. I'm out");
                stop();
            }
        } else {
            log("THIS SHOULD NEVER HAPPEN");
            stop();
        }
    }

    public int getPingCount() {
        return pingCount;
    }

    public boolean isPongTurn() {
        return pongTurn;
    }
}
