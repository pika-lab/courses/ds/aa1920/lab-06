package sd.lab.agency.behavioural.messages;

import sd.lab.agency.Agent;
import sd.lab.linda.textual.RegexTemplate;
import sd.lab.linda.textual.StringTuple;

import java.util.Objects;
import java.util.regex.Matcher;

// TODO notice, study, and understand this class
public class Message {

    public static final RegexTemplate MESSAGE_TEMPLATE = RegexTemplate.of(
            "^\\s*message\\s*\\{" +
            "\\s*to:\\s*(?<recipient>.*?)\\s*," +
            "\\s*from:\\s*(?<sender>.*?)\\s*," +
            "\\s*content:\\s*(?<payload>.*?)\\s*" +
            "\\}\\s*$"
    );

    public static RegexTemplate getTemplate() {
        return MESSAGE_TEMPLATE;
    }

    public static RegexTemplate getTemplateForRecipient(Agent recipient) {
        return getTemplateForRecipient(recipient.getFullName());
    }

    public static RegexTemplate getTemplateForRecipient(String recipient) {
        return RegexTemplate.of(
                MESSAGE_TEMPLATE.getRegex().pattern().replace("(?<recipient>.*?)", recipient)
        );
    }

    public static RegexTemplate getTemplateForSender(Agent sender) {
        return getTemplateForSender(sender.getFullName());
    }

    public static RegexTemplate getTemplateForSender(String sender) {
        return RegexTemplate.of(
                MESSAGE_TEMPLATE.getRegex().pattern().replace("(?<sender>.*?)", sender)
        );
    }

    public static Message of(String sender, String recipient, String content) {
        return new Message(sender, recipient, content);
    }

    public static Message of(Agent sender, String recipient, String content) {
        return new Message(sender.getFullName(), recipient, content);
    }

    public static Message of(String sender, Agent recipient, String content) {
        return new Message(sender, recipient.getFullName(), content);
    }

    public static Message of(Agent sender, Agent recipient, String content) {
        return new Message(sender.getFullName(), recipient.getFullName(), content);
    }

    public static Message parse(String input) {
        final Matcher m = MESSAGE_TEMPLATE.getRegex().matcher(input);
        if (m.matches()) {
            return Message.of(
                    m.group("sender"),
                    m.group("recipient"),
                    m.group("payload")
            );
        } else {
            throw new IllegalArgumentException("Cannot parse a Message from String: '" + input + "'");
        }
    }

    public static Message fromTuple(StringTuple tuple) {
        return parse(tuple.getValue());
    }

    private final String sender;
    private final String recipient;
    private final String content;

    private Message(String sender, String recipient, String content) {
        this.sender = Objects.requireNonNull(sender);
        this.recipient = Objects.requireNonNull(recipient);
        this.content = Objects.requireNonNull(content);
    }

    public String getSender() {
        return sender;
    }

    public String getRecipient() {
        return recipient;
    }

    public String getContent() {
        return content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message = (Message) o;
        return sender.equals(message.sender) &&
                recipient.equals(message.recipient) &&
                content.equals(message.content);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sender, recipient, content);
    }

    @Override
    public String toString() {
        return "message{" +
                "to:" + recipient +
                ", from:" + sender +
                ", content:" + content +
                '}';
    }

    public StringTuple asTuple() {
        return StringTuple.of(this.toString());
    }
}
