package sd.lab.agency.behavioural;

import sd.lab.agency.behavioural.impl.*;
import sd.lab.agency.behavioural.messages.Message;
import sd.lab.linda.textual.TextualSpace;
import sd.lab.utils.Action;
import sd.lab.utils.Action1;
import sd.lab.utils.Action2;

import java.time.Duration;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;
import java.util.function.Supplier;

@FunctionalInterface
public interface Behaviour {

    /// ACTUAL INTERFACE

    void execute(BehaviouralAgent agent) throws Exception;

    default boolean isPaused() {
        return false;
    }

    default boolean isOver() {
        return true;
    }

    default Behaviour deepClone() {
        return this;
    }

    /// STATIC FACTORIES

    static Behaviour of(Action<? extends Exception> action) {
        return agent -> action.execute();
    }

    static Behaviour of(Action1<BehaviouralAgent, ? extends Exception> action) {
        return action::execute;
    }

    static Behaviour sequence(Behaviour b, Behaviour... bs) {
        return new Sequence(b, bs);
    }

    static Behaviour allOf(Behaviour b, Behaviour... bs) {
        return new Parallel(Parallel.TerminationCriterion.ALL, b, bs);
    }

    static Behaviour anyOf(Behaviour b, Behaviour... bs) {
        return new Parallel(Parallel.TerminationCriterion.ANY, b, bs);
    }

    static Behaviour wait(Duration duration) {
        return new Wait(duration);
    }

    static <X> Behaviour lindaOperation(
            Function<BehaviouralAgent, String> nameGetter,
            Function<TextualSpace, CompletableFuture<X>> primitiveInvoker,
            Action2<BehaviouralAgent, X, Exception> resultHandler
    ) { return LindaOperation.of(nameGetter, primitiveInvoker, resultHandler); }

    static <X> Behaviour lindaOperation(
            String name,
            Function<TextualSpace, CompletableFuture<X>> primitiveInvoker,
            Action1<X, Exception> resultHandler
    ) { return lindaOperation(a -> name, primitiveInvoker, (a, r) -> resultHandler.execute(r)); }

    static Behaviour send(
            String recipient,
            String content,
            Action2<BehaviouralAgent, Message, Exception> onMessageSent
    ) { return Send.of(recipient, content, onMessageSent); }

    static Behaviour send(
            String recipient,
            String content,
            Action1<Message, Exception> onMessageSent
    ) { return send(recipient, content, (a, m) -> onMessageSent.execute(m)); }

    static Behaviour send(
            String recipient,
            String content
    ) { return send(recipient, content, m -> {}); }

    static Behaviour receive(
            Action2<BehaviouralAgent, Message, Exception> onMessageReceived
    ) { return Receive.of(onMessageReceived); }

    static Behaviour receive(
            Action1<Message, Exception> onMessageReceived
    ) { return receive((a, m) -> onMessageReceived.execute(m)); }

    /// DEFAULT OPERATORS

    default Behaviour addTo(BehaviouralAgent agent) {
        agent.addBehaviour(this);
        return this;
    }

    default Behaviour removeFrom(BehaviouralAgent agent) {
        agent.removeBehaviour(this);
        return this;
    }

    default Behaviour andThen(Behaviour b, Behaviour... bs) {
        return new Sequence(this, b, bs);
    }

    default Behaviour andThen(Action<? extends Exception> action) {
        return andThen(Behaviour.of(action));
    }

    default Behaviour andThen(Action1<BehaviouralAgent, ? extends Exception> action) {
        return andThen(Behaviour.of(action));
    }

    default Behaviour repeatWhile(Supplier<Boolean> condition) {
        return DoWhile.of(this, condition);
    }

    default Behaviour repeatUntil(Supplier<Boolean> condition) {
        return repeatWhile(() -> !condition.get());
    }

    default Behaviour repeatForEver() {
        return repeatWhile(() -> true);
    }
}
