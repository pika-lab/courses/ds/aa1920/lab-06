package sd.lab.agency.behavioural.impl;

import sd.lab.agency.behavioural.Behaviour;
import sd.lab.agency.behavioural.BehaviouralAgent;

import java.time.Duration;
import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;

public class Wait implements Behaviour {

    private final Duration duration;
    private boolean started;
    private OffsetDateTime clock;
    private boolean ended;

    public Wait(Duration duration) {
        this.duration = duration;
    }

    @Override
    public Behaviour deepClone() {
        return new Wait(duration);
    }

    @Override
    public void execute(BehaviouralAgent agent) {
        // TODO implement
        throw new IllegalStateException("not implemented");
    }

    @Override
    public boolean isOver() {
        return ended;
    }

    private long getElapsedMillis() {
        return ChronoUnit.MILLIS.between(clock, OffsetDateTime.now());
    }
}
