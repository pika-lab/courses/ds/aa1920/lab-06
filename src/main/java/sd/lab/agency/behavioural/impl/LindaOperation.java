package sd.lab.agency.behavioural.impl;

import sd.lab.agency.behavioural.Behaviour;
import sd.lab.agency.behavioural.BehaviouralAgent;
import sd.lab.linda.textual.TextualSpace;
import sd.lab.utils.Action2;

import java.util.concurrent.CompletableFuture;
import java.util.function.Function;

public abstract class LindaOperation<R> extends Await<R> {

    @Override
    public final CompletableFuture<R> getPromise(BehaviouralAgent agent) {
        // TODO implement
        throw new IllegalStateException("not implemented");
    }

    public abstract CompletableFuture<R> invokeOnTupleSpace(BehaviouralAgent agent, TextualSpace tupleSpace);

    public abstract String getTupleSpaceName(BehaviouralAgent agent);

    public static <X> LindaOperation<X> of(
                Function<BehaviouralAgent, String> nameGetter,
                Function<TextualSpace, CompletableFuture<X>> primitiveInvoker,
                Action2<BehaviouralAgent, X, Exception> resultHandler
            ) {
        return new LindaOperation<X>() {
            @Override
            public CompletableFuture<X> invokeOnTupleSpace(BehaviouralAgent agent, TextualSpace tupleSpace) {
                return primitiveInvoker.apply(tupleSpace);
            }

            @Override
            public String getTupleSpaceName(BehaviouralAgent agent) {
                return nameGetter.apply(agent);
            }

            @Override
            public void onResult(BehaviouralAgent agent, X result) throws Exception {
                resultHandler.execute(agent, result);
            }

            @Override
            public Behaviour deepClone() {
                return LindaOperation.of(nameGetter, primitiveInvoker, resultHandler);
            }
        };
    }
}
