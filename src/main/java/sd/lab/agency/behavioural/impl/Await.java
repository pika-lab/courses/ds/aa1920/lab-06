package sd.lab.agency.behavioural.impl;

import sd.lab.agency.behavioural.Behaviour;
import sd.lab.agency.behavioural.BehaviouralAgent;

import java.util.concurrent.CompletableFuture;

public abstract class Await<T> implements Behaviour {

    private enum Phase { CREATED, PAUSED, COMPLETED, DONE }

    private Phase phase = Phase.CREATED;
    private CompletableFuture<T> promiseCache;

    @Override
    public void execute(BehaviouralAgent agent) throws Exception {
        // TODO implement
        throw new IllegalStateException("not implemented");
    }

    public abstract void onResult(BehaviouralAgent agent, T result) throws Exception;

    public abstract CompletableFuture<T> getPromise(BehaviouralAgent agent);


    @Override
    public boolean isPaused() {
        return phase == Phase.PAUSED;
    }

    @Override
    public boolean isOver() {
        return phase == Phase.DONE;
    }

    @Override
    public Behaviour deepClone() {
        throw new IllegalStateException("You must override the deepClone method in class " + this.getClass().getName());
    }
}
