package sd.lab.agency.behavioural.impl;

import sd.lab.agency.behavioural.Behaviour;
import sd.lab.agency.behavioural.BehaviouralAgent;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.stream.Collectors;

public class Parallel implements Behaviour {

    private final LinkedList<Behaviour> subBehaviours = new LinkedList<>();
    private final TerminationCriterion terminationCriterion;
    private boolean shortCircuitEnd = false;

    public Parallel(TerminationCriterion terminationCriterion, Collection<Behaviour> bs) {
        this.terminationCriterion = terminationCriterion;
        if (bs.isEmpty()) throw new IllegalArgumentException();
        subBehaviours.addAll(bs);
    }

    public Parallel(TerminationCriterion terminationCriterion, Behaviour b, Behaviour... bs) {
        this.terminationCriterion = terminationCriterion;
        subBehaviours.add(b);
        subBehaviours.addAll(Arrays.asList(bs));
    }

    @Override
    public Behaviour deepClone() {
        return new Parallel(terminationCriterion, subBehaviours.stream().map(Behaviour::deepClone).collect(Collectors.toList()));
    }

    @Override
    public void execute(BehaviouralAgent agent) throws Exception {
        // TODO implement
        throw new IllegalStateException("not implemented");
    }

    @Override
    public boolean isOver() {
        if (terminationCriterion == TerminationCriterion.ALL) {
            return subBehaviours.isEmpty();
        } else {
            return shortCircuitEnd || subBehaviours.isEmpty();
        }
    }

    @Override
    public boolean isPaused() {
        return subBehaviours.stream().allMatch(Behaviour::isPaused);
    }

    public enum TerminationCriterion {ANY, ALL}

}
