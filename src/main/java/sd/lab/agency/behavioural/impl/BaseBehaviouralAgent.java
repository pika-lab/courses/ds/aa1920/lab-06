package sd.lab.agency.behavioural.impl;

import sd.lab.agency.behavioural.Behaviour;
import sd.lab.agency.behavioural.BehaviouralAgent;
import sd.lab.agency.impl.BaseAgent;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Queue;

public abstract class BaseBehaviouralAgent extends BaseAgent implements BehaviouralAgent {

    private final Queue<Behaviour> toDoList = new LinkedList<>();

    protected BaseBehaviouralAgent(String name) {
        super(name);
    }

    @Override
    public Queue<Behaviour> getToDoList() {
        return toDoList;
    }

    @Override
    public final void onBegin() {
        setup();
    }

    @Override
    public void onRun() throws Exception {
        // TODO implement
        throw new IllegalStateException("not implemented");
    }

    @Override
    public void setup() {
        // does nothing by default
    }

    @Override
    public final void onEnd() {
        tearDown();
    }

    @Override
    public void tearDown() {
        // does nothing by default
    }

    @Override
    public BehaviouralAgent addBehaviour(Collection<? extends Behaviour> behaviours) {
        if (behaviours.size() > 0) {
            toDoList.addAll(behaviours);
            resumeIfPaused();
        }
        return this;
    }

    @Override
    public BehaviouralAgent removeBehaviour(Collection<? extends Behaviour> behaviours) {
        if (behaviours.size() > 0) {
            toDoList.addAll(behaviours);
            resumeIfPaused();
        }
        return this;
    }
}
