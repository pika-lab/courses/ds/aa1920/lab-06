package sd.lab.agency;

import java.time.Duration;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeoutException;

public interface Agent {
    void onBegin() throws Exception;

    void onRun() throws Exception;

    void onEnd() throws Exception;

    AndThen onUncaughtError(Exception e);

    Agent start();
    Agent stop();
    Agent pause();
    Agent resume();
    Agent restart();
    Agent resumeIfPaused();

    String getFullName();
    String getLocalName();

    Agent await(Duration duration) throws InterruptedException, ExecutionException, TimeoutException;
    Agent await() throws InterruptedException, ExecutionException, TimeoutException;

    Environment getEnvironment();
    void setEnvironment(Environment environment);

    default ExecutorService getEngine() {
        return getEnvironment() != null ? getEnvironment().getEngine() : null;
    }

    void log(Object format, Object... args);
}
