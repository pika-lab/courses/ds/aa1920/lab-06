package sd.lab.agency;

public enum AndThen {
    CONTINUE, RESTART, PAUSE, STOP
}
