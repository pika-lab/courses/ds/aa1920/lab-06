package sd.lab.agency.impl;

import sd.lab.agency.Agent;
import sd.lab.agency.AndThen;
import sd.lab.agency.Environment;

import java.time.Duration;
import java.util.EnumSet;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public abstract class BaseAgent implements Agent {

    protected enum States {
        CREATED, STARTED, RUNNING, PAUSED, STOPPED
    }

    private final String name;
    private final CompletableFuture<Void> termination = new CompletableFuture<>();
    private States state = States.CREATED;
    private AndThen nextOperation = null;
    private Environment environment;

    protected BaseAgent(String name) {
        this.name = Optional.ofNullable(name).orElseGet(() -> getClass().getSimpleName() + "#" + System.identityHashCode(this));
    }

    private void ensureCurrentStateIs(States state) {
        ensureCurrentStateIs(EnumSet.of(state));
    }

    private void ensureCurrentStateIs(EnumSet<States> states) {
        if (!currentStateIsOneOf(states)) {
            final RuntimeException e = new IllegalStateException("Illegal state: " + this.state + ", expected: " + states);
            e.printStackTrace();
            throw e;
        }
    }

    private boolean currentStateIs(States state) {
        return Objects.equals(this.state, state);
    }

    private boolean currentStateIsOneOf(EnumSet<States> states) {
        return states.contains(this.state);
    }

    private void doStateTransition(AndThen whatToDo) {
        switch (state) {
            case CREATED:
                doStateTransitionFromCreated(whatToDo);
                break;
            case STARTED:
                doStateTransitionFromStarted(whatToDo);
                break;
            case RUNNING:
                doStateTransitionFromRunning(whatToDo);
                break;
            case PAUSED:
                doStateTransitionFromPaused(whatToDo);
                break;
            case STOPPED:
                doStateTransitionFromStopped(whatToDo);
                break;
            default: throw new IllegalStateException("Illegal state: " + state);
        }
    }

    protected void doStateTransitionFromCreated(AndThen whatToDo){
        switch (whatToDo) {
            case CONTINUE:
                state = States.STARTED;
                doBegin();
                break;
            default: throw new IllegalArgumentException("Unexpected transition: " + state + " -" + whatToDo + "-> ???");
        }
    }

    protected void doStateTransitionFromStarted(AndThen whatToDo){
        doStateTransitionFromRunning(whatToDo);
    }

    protected void doStateTransitionFromRunning(AndThen whatToDo){
        switch (whatToDo) {
            case PAUSE:
                state = States.PAUSED;
                break;
            case RESTART:
                state = States.STARTED;
                doBegin();
                break;
            case STOP:
                state = States.STOPPED;
                doEnd();
                break;
            case CONTINUE:
                state = States.RUNNING;
                doRun();
                break;
            default: throw new IllegalArgumentException("Unexpected transition: " + state + " -" + whatToDo + "-> ???");
        }
    }

    protected void doStateTransitionFromPaused(AndThen whatToDo){
        doStateTransitionFromRunning(whatToDo);
    }

    protected void doStateTransitionFromStopped(AndThen whatToDo){
        switch (whatToDo) {
            case RESTART:
                state = States.STARTED;
                doBegin();
                break;
            case STOP:
            case CONTINUE:
                state = null;
                termination.complete(null);
                break;
            default: throw new IllegalArgumentException("Unexpected transition: " + state + " -" + whatToDo + "-> ???");
        }
    }

    private void doBegin() {
        if (getEnvironment() == null) {
            throw new IllegalStateException();
        }
        ensureCurrentStateIs(States.STARTED);
        getEngine().submit(this::begin);
    }

    private void begin() {
        nextOperation = AndThen.CONTINUE;
        try {
            onBegin();
        } catch (Exception e) {
            nextOperation = onUncaughtError(e);
        } finally {
            doStateTransition(nextOperation);
        }
    }

    @Override
    public void onBegin() throws Exception {
        // it's a callback and it does nothing by default (must be inherited)
    }

    private void doRun() {
        ensureCurrentStateIs(EnumSet.of(States.PAUSED, States.RUNNING));
        getEngine().submit(this::run);
    }

    private void run() {
        nextOperation = AndThen.CONTINUE;
        try {
            onRun();
        } catch (Exception e) {
            nextOperation = onUncaughtError(e);
        } finally {
            doStateTransition(nextOperation);
        }
    }

    // TODO keep this abstract in order to force subclasses to provide some behaviour
    public abstract void onRun() throws Exception;

    private void doEnd() {
        ensureCurrentStateIs(EnumSet.of(States.STARTED, States.PAUSED, States.RUNNING, States.STOPPED));
        getEngine().submit(this::end);
    }

    private void end() {
        nextOperation = AndThen.CONTINUE;
        try {
            onEnd();
        } catch (Exception e) {
            nextOperation = onUncaughtError(e);
        } finally {
            doStateTransition(nextOperation);
        }
    }

    @Override
    public void onEnd() throws Exception {
        // does nothing by default
    }

    @Override
    public AndThen onUncaughtError(Exception e) {
        // by default, it simply print stacktrace and then stops the agent (can be overridden)
        e.printStackTrace();
        return AndThen.STOP;
    }

    @Override
    public Agent start() {
        ensureCurrentStateIs(States.CREATED);
        doStateTransition(AndThen.CONTINUE);
        return this;
    }

    @Override
    public Agent resume() {
        ensureCurrentStateIs(States.PAUSED);
        doStateTransition(AndThen.CONTINUE);
        return this;
    }

    @Override
    public Agent pause() {
        ensureCurrentStateIs(EnumSet.of(States.STARTED, States.RUNNING, States.PAUSED));
        nextOperation = AndThen.PAUSE;
        return this;
    }

    @Override
    public Agent stop() {
        ensureCurrentStateIs(EnumSet.of(States.STARTED, States.RUNNING, States.PAUSED));
        if (currentStateIs(States.PAUSED)) {
            doStateTransition(AndThen.STOP);
        } else {
            nextOperation = AndThen.STOP;
        }
        return this;
    }

    @Override
    public Agent restart() {
        ensureCurrentStateIs(EnumSet.complementOf(EnumSet.of(States.CREATED)));
        nextOperation = AndThen.RESTART;
        return this;
    }

    @Override
    public final Agent resumeIfPaused() {
        if (currentStateIs(States.PAUSED)) {
            doStateTransition(AndThen.CONTINUE);
        }
        return this;
    }

    public Environment getEnvironment() {
        return environment;
    }

    public void setEnvironment(Environment environment) {
        this.environment = Objects.requireNonNull(environment);
    }

    @Override
    public final void log(Object format, Object... args) {
        System.out.printf("[" + getFullName() +"] " + format + "\n", args);
    }

    @Override
    public String getFullName() {
        return getEnvironment() != null ? getEnvironment().getFullNameFor(getLocalName()) : null;
    }

    @Override
    public String getLocalName() {
        return name;
    }

    @Override
    public Agent await(Duration duration) throws InterruptedException, ExecutionException, TimeoutException {
        termination.get(duration.toMillis(), TimeUnit.MILLISECONDS);
        return this;
    }

    @Override
    public Agent await() throws InterruptedException, ExecutionException, TimeoutException {
        termination.get(Long.MAX_VALUE, TimeUnit.MILLISECONDS);
        return this;
    }

}

